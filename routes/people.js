var people  = [
  { "id": 1, "firstName": "James", "lastName": "King" },
  { "id": 1, "firstName": "Julie", "lastName": "Taylor" }
];


exports.findAll = function (req, res, next) {
    var name = req.query.name;
    if (name) {
        res.send(people.filter(function(employee) {
            return (people.firstName + ' ' + people.lastName).toLowerCase().indexOf(name.toLowerCase()) > -1;
        }));
    } else {
        res.send(people);
    }
};
