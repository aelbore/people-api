/* global people */
var express = require('express'),
    people = require('./routes/people'),
    app = express(),
    port = process.env.PORT || 5001;

app.use(express.static('www'));

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.get('/people', people.findAll);

app.set('port', port);

app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});